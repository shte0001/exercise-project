// Copyright (c) 2021 Helmholtz-Zentrum Dresden - Rossendorf e.V.
//
// SPDX-License-Identifier: MIT

#include <gtest/gtest.h>
#include <cstdlib>

#include "src/helloworld.h"

namespace {

    TEST(getUserNameViaEnv, HandleEnvUnset) {
        // Test if function returns World if environment variable unset

        // Make sure environment variable is unset
        char* envVarNameChar;
        std::string envVarName = std::string{"GITLAB_USER_NAME"};
        envVarNameChar = &envVarName[0];
        unsetenv(envVarNameChar);

        // Assert that we get the expected result
        EXPECT_EQ(std::string{"World"}, getUserNameViaEnv());
    }

    TEST(getUserNameViaEnv, HandleEnvSet) {
        // Test if function returns World if environment variable unset

        // Set the environment variable to the desired value
        char* envVarNameChar;
        char* envVarValChar;
        std::string envVarName = std::string{"GITLAB_USER_NAME"};
        std::string envVarVal = std::string{"Jane Doe"};
        envVarNameChar = &envVarName[0];
        envVarValChar = &envVarVal[0];
        setenv(envVarNameChar, envVarValChar, true);

        // Assert that function output and environment variable are similar
        EXPECT_EQ(envVarVal, getUserNameViaEnv());

        // Clean up
        unsetenv(envVarNameChar);
    }

}  // namespace
